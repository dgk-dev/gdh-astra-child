<?php


class WC_Payment_Payu_Latam_API_SPEI extends WC_Payment_Gateway
{
    public function __construct()
    {
        $this->id = 'payu_latam_api_spei';
        $this->icon = woo_payu_latam_sdk_pls()->plugin_url . 'assets/images/logoPayU.png';
        $this->method_title = __('PayU SPEI', 'woo-payu-latam-sdk');
        $this->method_description = __('Pago a través de SPEI', 'woo-payu-latam-sdk');
        $this->description  = $this->get_option( 'description' );
        $this->has_fields = false;
        $this->supports = ['products'];
        $this->init_form_fields();
        $this->init_settings();
        $this->title = $this->get_option('title');
        $this->description  = $this->get_option( 'description' );

        $this->merchant_id  = $this->get_option( 'merchant_id' );
        $this->account_id  = $this->get_option( 'account_id' );
        $this->apikey  = $this->get_option( 'apikey' );
        $this->apilogin  = $this->get_option( 'apilogin' );
        $this->isTest = (boolean)$this->get_option('environment');
        $this->currency = get_woocommerce_currency();
        $this->debug = $this->get_option('debug');

        add_action( 'woocommerce_thankyou', array($this, 'add_spei_payment_iframe'), 4 );
        add_action('woocommerce_api_'.strtolower(get_class($this)), array($this, 'confirmation_ipn'));
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
    }

    public function is_available()
    {
        return parent::is_available() && woo_payu_latam_sdk_pls()->getDefaultCountry() === 'MX';
    }

    public function init_form_fields()
    {

        $this->form_fields = require( dirname( __FILE__ ) . '/admin/payu-settings-general.php' );
    }

    public function admin_options()
    {
        ?>
        <h3><?php echo $this->title; ?></h3>
        <p><?php echo $this->method_description; ?></p>
        <table class="form-table">
            <?php $this->generate_settings_html(); ?>
        </table>
        <?php
    }

    public function payment_fields()
    {

        if ( $description = $this->get_description())
            echo wp_kses_post( wpautop( wptexturize( $description ) ) );
    }

    public function process_payment($order_id)
    {
        $order = new WC_Order($order_id);
        $dataSign = [
            'referenceCode' => $order->get_id(),
            'tx_value' => $order->get_total(),
            'currency' => $order->get_currency()
        ];
        $products = array();
        foreach ( $order->get_items() as  $item_key => $item_values ) {
            $item_data = $item_values->get_data();
            $products[] = $item_data['name'];
        }
        $api_request = array(
            'language' => 'es',
            'command' => 'SUBMIT_TRANSACTION',
            'merchant' => array(
                'apiKey' => $this->apikey,
                'apiLogin' => $this->apilogin
            ),
            'transaction' => array(
                'order' => array(
                    'accountId' => $this->account_id,
                    'referenceCode' => $order->get_id(),
                    'description' => get_bloginfo('name').": " . implode(', ', $products),
                    'language' => 'es',
                    'signature' => $this->get_sign_validate($dataSign),
                    'notifyUrl' => $this->getUrlNotify(),
                    'additionalValues' => array(
                        'TX_VALUE' => array(
                            'value' => $order->get_total(),
                            'currency' => $order->get_currency()
                        ),
                        'TX_TAX' => array(
                            'value' => $order->get_total_tax(),
                            'currency' => $order->get_currency()
                        ),
                        'TX_TAX_RETURN_BASE' => array(
                            'value' => 0,
                            'currency' => $order->get_currency()
                        )
                    ),
                    'buyer' => array(
                        'fullName' => $order->get_shipping_first_name() ? $order->get_shipping_first_name() . " " . $order->get_shipping_last_name() : $order->get_billing_first_name() . " " . $order->get_billing_last_name(),
                        'emailAddress' => $order->get_billing_email()
                    )
                ),
                'type' => 'AUTHORIZATION_AND_CAPTURE',
                'paymentMethod' => 'SPEI',
                'expirationDate' => $this->dateExpire(),
                'paymentCountry' => woo_payu_latam_sdk_pls()->getDefaultCountry(),
                'ipAddress' => $this->getIP(),
            ),
            'test' => false
        );
        $url = $this->isTest ? 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi' : 'https://api.payulatam.com/payments-api/4.0/service.cgi';
        
        $request = wp_remote_post($url, array(
            'headers'     => array('Content-Type' => 'application/json; charset=utf-8'),
            'body'        => json_encode($api_request),
            'method'      => 'POST',
            'data_format' => 'body',
        ));
        if(is_wp_error($request)){
            wc_add_notice('e:' . $request->get_error_message(), 'error' );
            return false;
        }else{
            $body     = wp_remote_retrieve_body($request);
            $xml  = simplexml_load_string($body);
            $state = (string)$xml->transactionResponse->state;
            update_post_meta($order_id, '_xml_string_payu_spei_response', (string)$xml);
            if($state=="DECLINED"){
                $error = (string)$xml->transactionResponse->paymentNetworkResponseErrorMessage;
                woo_payu_latam_sdk_pls()->log($xml);
                wc_add_notice($error, 'error' );
                return false;
            }elseif($state == "PENDING"){
                $spei_url = (string)$xml->transactionResponse->extraParameters->entry[5]->string[1];
                update_post_meta($order_id, '_payu_latam_spei_url', $spei_url);
                wc_reduce_stock_levels($order_id);
                WC()->cart->empty_cart();
                return [
                    'result' => 'success',
                    'redirect' => $this->get_return_url( $order )
                ];
            }
        }
        return parent::process_payment($order_id);
    }

 
    function add_spei_payment_iframe( $order_id ) {
        $order = wc_get_order( $order_id );
        $payment_url = get_post_meta($order_id, '_payu_latam_spei_url', true);
        if($payment_url && $order->has_status('pending')): ?>
            <p><strong>Enseguida encontrarás la información para pago:</strong></p>
            <small>Una copia de esta información fue enviada a tu correo electrónico</small>
            <style>
                .payu-iframe{
                    height: 2400px;
                    width: 100%;
                }  
                @media (min-width: 810px) {
                    .payu-iframe{
                        height: 2100px;
                    }
                }
                @media (min-width: 1240px) {
                    .payu-iframe{
                        height: 2000px;
                    }
                }
            </style>
            <iframe class="payu-iframe" id="payu_latam_spei_iframe" src="<?php echo $payment_url; ?>" frameborder="0"></iframe>
        <?php endif;
    }

    public function confirmation_ipn()
    {
        $body = file_get_contents('php://input');
        parse_str($body, $data);

        if (empty($data['referenceCode']))
            return;

        $order_id = $data['referenceCode'];
        $order = new WC_Order($order_id);

        $signature_payu = $data['signature'];
        $dataSign = [
            'referenceCode' => $data['referenceCode'],
            'tx_value' => $data['TX_VALUE'],
            'currency' => $data['currency']
        ];

        $signatureOrder = $this->get_sign_validate($dataSign);

        woo_payu_latam_sdk_pls()->log("signatureOrder: $signatureOrder signature_payu: $signature_payu");

        $transaction_id = $data['transactionId'];
        $transaction_state = $data['transactionState'];

        if ($transaction_state === '7' || $signatureOrder !== $signature_payu)
            return;

        if ($transaction_state === '4'){
            $order->payment_complete($transaction_id);
            $order->add_order_note(sprintf(__('Successful payment (Transaction ID: %s)',
                'woo-payu-latam-sdk'), $transaction_id));
        }elseif ($transaction_state !== '7'  && $transaction_state !== '4'){
            $order->update_status('failed');
            if(!isset($data['message']))
                $order->add_order_note($data['message']);
        }

        header("HTTP/1.1 200 OK");
    }

    public function formatted_amount($amount, $decimals = 2)
    {
        $amount = number_format($amount, $decimals,'.','');
        return $amount;
    }

    public function get_sign_validate(array $data = [])
    {
        return md5(
            $this->apikey . "~" .
            $this->merchant_id . "~" .
            $data['referenceCode'] ."~".
            $data['tx_value']."~".
            $data['currency']
        );
    }

    public function getUrlNotify()
    {
        $url = trailingslashit(get_bloginfo( 'url' )) . trailingslashit('wc-api') . strtolower(get_parent_class($this));
        return $url;
    }

    public function dateExpire()
    {

        $today = $this->dateCurrent();
        $day = $this->getDay();

        $addDay = 1;

        if ($day == 5)
            $addDay += 2;
        if ($day == 6)
            $addDay += 1;

        $today = strtotime ( "+$addDay days" , strtotime ( $today ) );
        $today = date ( 'Y-m-d\TH:i:s' , $today );

        return $today;

    }
    public function getDay()
    {
        $today = $this->dateCurrent();
        $weekDay = date('w', strtotime($today));
        return $weekDay;
    }

    public function dateCurrent()
    {
        $dateCurrent = date('Y-m-d\TH:i:s', current_time( 'timestamp' ));

        return $dateCurrent;
    }

    public function getIP()
    {
        return ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['REMOTE_ADDR'] == '::' ||
            !preg_match('/^((?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9]).){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])$/m',
                $_SERVER['REMOTE_ADDR'])) ? '127.0.0.1' : $_SERVER['REMOTE_ADDR'];
    }
}
