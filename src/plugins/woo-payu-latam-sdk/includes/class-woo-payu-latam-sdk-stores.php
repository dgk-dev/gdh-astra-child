<?php


class WC_Payment_Payu_Latam_SDK_Stores extends WC_Payment_Gateway
{
    public function __construct()
    {
        $this->id = 'payu_latam_sdk_stores';
        $this->icon = woo_payu_latam_sdk_pls()->plugin_url . 'assets/images/logoPayU.png';
        $this->method_title = __('PayU Stores', 'woo-payu-latam-sdk');
        $this->method_description = __('Paga a través de distintas tiendas de conveniencia en México', 'woo-payu-latam-sdk');
        $this->description  = $this->get_option( 'description' );
        $this->has_fields = false;
        $this->supports = ['products'];
        $this->init_form_fields();
        $this->init_settings();
        $this->title = $this->get_option('title');
        $this->description  = $this->get_option( 'description' );
        add_action( 'woocommerce_thankyou', array($this, 'add_cash_payment_iframe'), 4 );

        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
    }

    public function is_available()
    {
        return parent::is_available() && woo_payu_latam_sdk_pls()->getDefaultCountry() === 'MX';
    }

    public function init_form_fields()
    {

        $this->form_fields = [
            'enabled' => [
                'title' => __('Enable/Disable', 'woo-payu-latam-sdk'),
                'type' => 'checkbox',
                'label' => __('Enable Stores', 'woo-payu-latam-sdk'),
                'default' => 'no'
            ],
            'title' => [
                'title' => __('Title', 'woo-payu-latam-sdk'),
                'type' => 'text',
                'description' => __('It corresponds to the title that the user sees during the checkout', 'woo-payu-latam-sdk'),
                'default' => __('Tiendas de conveniencia', 'woo-payu-latam-sdk'),
                'desc_tip' => true
            ],
            'description' => [
                'title' => __('Description', 'woo-payu-latam-sdk'),
                'type' => 'textarea',
                'description' => __('It corresponds to the description that the user will see during the checkout', 'woo-payu-latam-sdk'),
                'default' => __('Paga a través de distintas tiendas de conveniencia en México', 'woo-payu-latam-sdk'),
                'desc_tip' => true
            ]
        ];
    }

    public function admin_options()
    {
        $settings_url = admin_url().'admin.php?page=wc-settings&tab=checkout&section=payu_latam_sdk_pls';
        ?>
        <h3><?php echo $this->title; ?></h3>
        <p><?php echo $this->method_description; ?></p>
        <p><?php echo __('Este método utiliza la', 'woo-payu-latam-sdk'); ?> <a href="<?php echo $settings_url; ?>"><?php echo __('configuración general', 'woo-payu-latam-sdk'); ?></a></p>
        <table class="form-table">
            <?php $this->generate_settings_html(); ?>
        </table>
        <?php
    }

    public function payment_fields()
    {

        if ( $description = $this->get_description())
            echo wp_kses_post( wpautop( wptexturize( $description ) ) );

        ?>
        <div class="stores-payu-latam-sdk">
            <div class="form-row-wide">
                <input type="radio" name="payu_latam_store" id="oxxo" value="OXXO"/>
                <label for="oxxo" style="vertical-align: middle"><span class="ico_pm_OXXO"></span></label>
            </div>
            <div class="form-row-wide">
                <input type="radio" name="payu_latam_store" id="seven_eleven" value="SEVEN_ELEVEN"/>
                <label for="seven_eleven" style="vertical-align: middle"><span class="ico_pm_SEVEN_ELEVEN"></span></label>
            </div>
            <div class="form-row-wide">
                <input type="radio" name="payu_latam_store" id="others_cash_mx" value="OTHERS_CASH_MX"/>
                <label for="others_cash_mx" style="vertical-align: middle"><span class="ico_pm_OTHERS_CASH_MX_FARM_BENAVIDES"></span><span class="ico_pm_OTHERS_CASH_MX_FARM_AHORRO"></span></label>
            </div>
            <div class="form-row-wide">
                <input type="radio" name="payu_latam_store" id="bancomer" value="BANCOMER"/>
                <label for="bancomer" style="vertical-align: middle"><span class="ico_pm_BANCOMER"></span></label>
            </div>
        </div>
        <?php
    }

    public function process_payment($order_id)
    {
        if(!$_POST['payu_latam_store']){
            wc_add_notice(__('Selecciona una tienda para pago', 'woo-payu-latam-sdk'), 'error' );
            return false;
        }
        
        $params = $_POST;
        $params['id_order'] = $order_id;
        $params['payu-latam-sdk-payment-method'] = $_POST['payu_latam_store'];
        $payment = new Payu_Latam_SDK_PLS();
        $data = $payment->doPayment($params, false);
        
        if($data['status']){
            $order = new WC_Order( $order_id );
            wc_reduce_stock_levels($order_id);
            WC()->cart->empty_cart();
            update_post_meta($order_id, '_payu_latam_cash_url', $data['url']);
            return [
                'result' => 'success',
                'redirect' => $this->get_return_url( $order )
            ];
        }else{
            wc_add_notice($data['message'], 'error' );
            woo_payu_latam_sdk_pls()->log($data['message']);
        }

        return parent::process_payment($order_id);
    }

 
    function add_cash_payment_iframe( $order_id ) {
        $order = wc_get_order( $order_id );
        $payment_url = get_post_meta($order_id, '_payu_latam_cash_url', true);
        if($payment_url && $order->has_status('pending')): ?>
            <p><strong>Enseguida encontrarás la información para pago:</strong></p>
            <small>Una copia de esta información fue enviada a tu correo electrónico</small>
            <style>
                .payu-iframe{
                    height: 2100px;
                    width: 100%;
                }  
                @media (min-width: 810px) {
                    .payu-iframe{
                        height: 1700px;
                    }
                }
                @media (min-width: 1240px) {
                    .payu-iframe{
                        height: 1500px;
                    }
                }
                
            </style>
            <iframe class="payu-iframe" id="payu_latam_cash_iframe" src="<?php echo $payment_url; ?>" frameborder="0" style="overflow:hidden;"></iframe>
        <?php endif;
    }
}
