<?php
//Google analytics tag
add_action('wp_head', 'gdh_google_analytics', 7);
function gdh_google_analytics()
{
?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W8X6PH2');</script>
    <!-- End Google Tag Manager -->
<?php
}

// add_action('wp_body_open', 'gdh_google_analytics_nojs');
function gdh_google_analytics_nojs()
{
?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8X6PH2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php
}

/**
 * Funciones avanzadas de google analytics para ecommerce
 * 
 */

//Transacción completa
add_action('woocommerce_thankyou', 'gdh_ga_add_transaction', 10, 1);
function gdh_ga_add_transaction($order_id)
{
    $order = wc_get_order($order_id);

    if (!$order || get_post_meta($order_id, '_ga_tracked', true) == 1) {
        return;
    }
    $items = array();
    if ($order->get_items()) {
        foreach ($order->get_items() as $item) {
            $items[] = gdh_ga_add_item($item);
        }
    }

    $ga_code = "dataLayer.push({
        'event': 'purchase',
        'ecommerce': {
            'transaction_id': '" . esc_js($order_id) . "',
            'affiliation': 'Green Doctor Home',
            'value': '" . esc_js(number_format( (float) $order->get_total() - $order->get_shipping_total(), wc_get_price_decimals(), '.', '' )) . "',
            'shipping': '" . esc_js(number_format( (float) $order->get_shipping_total(), wc_get_price_decimals(), '.', '' )) . "',
            'currency': '" . esc_js($order->get_currency()) . "',
            'coupon': '" . esc_js(implode(', ', $order->get_coupon_codes())) . "',
            'items':" . json_encode($items). " 
        }
    });";


    // Mark the order as tracked.
    update_post_meta($order_id, '_ga_tracked', 1);
    echo "<script>" . $ga_code . "</script>";
}

function gdh_ga_add_item($item)
{
    $product = $item->get_product();
    $categories = array();
    $category_terms =  get_the_terms($item->get_product_id(), 'product_cat');
    foreach ($category_terms as $category_term) {
        $categories[] = $category_term->name;
    }
    

    $_item = array(
        'item_name' => $item->get_name(),
        'item_id' => $product->get_sku() ? $product->get_sku() : ('#' . $item->get_product_id()),
        'price' => esc_js($product->get_price()),
        'item_brand' => 'Green Doctor',
        'item_category' => implode(', ', array_filter($categories)),
        'quantity' =>  abs($item->get_quantity()),
        'coupon' => "",
        'discount' => ""
    );

    return $_item;
}


// //Agregar al carrito
add_action( 'woocommerce_after_add_to_cart_button', 'gdh_ga_add_to_cart');
function gdh_ga_add_to_cart()
{
    global $product;
    $categories = array();
    $category_terms = get_the_terms($product->get_id(), 'product_cat');
    foreach ($category_terms as $category_term) {
        $categories[] = $category_term->name;
    }

    $ga_code = "dataLayer.push({
      'event': 'add_to_cart',
      'ecommerce': {
        'items': [{
            'item_name': '" . esc_js($product->get_title()) . "',
            'item_id': '" . esc_js($product->get_sku() ? $product->get_sku() : ('#' . $product->get_id())) . "',
            'price': '" . esc_js($product->get_price()) . "',
            'item_brand': 'Green Doctor',
            'item_category': '" . esc_js(implode(', ', array_filter($categories))) . "',
            'quantity': $( 'input.qty' ).length ? $( 'input.qty' ).val() : '1',
            'index': '1',
        }]
      }
    });";

    $selector = '.single_add_to_cart_button';
    gdh_ga_set_add_to_cart_code($selector, $ga_code);
}

function gdh_ga_set_add_to_cart_code($selector, $ga_code)
{
?>
    <script>
        (function($) {
            $(document.body).on('click', '<?php echo $selector ?>', function(e) {
                <?php echo $ga_code ?>
            });
        })(jQuery);
    </script>
<?php
}

add_action('wp_footer', 'gdh_add_to_cart_grids_script');

function gdh_add_to_cart_grids_script(){
    ?>
    <script>
        (function($) {
            $('body').on('click', '.ajax_add_to_cart', function(e) {
                product_id = $(this).attr('data-product_id');
                console.log(product_id);
                $.ajax({
                    type: "post",
                    url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                    data: {
                        action: 'gdh_get_item_data',
                        product_id: product_id
                    },
                    success: function(item){
                        dataLayer.push({
                            'event': 'add_to_cart',
                            'ecommerce': {
                                'items': [{
                                    'item_name': item.item_name,
                                    'item_id': item.item_id,
                                    'price': item.price,
                                    'item_brand': item.item_brand,
                                    'item_category': item.item_category,
                                    'quantity': item.quantity,
                                    'index': item.index
                                }]
                            }
                        });
                    }
                });
            });
        })(jQuery);
    </script>
<?php
}

add_action( 'wp_ajax_nopriv_gdh_get_item_data', 'gdh_get_item_data_ajax' );
add_action( 'wp_ajax_gdh_get_item_data', 'gdh_get_item_data_ajax' );

function gdh_get_item_data_ajax(){
    global $woocommerce;
    $product = wc_get_product($_POST['product_id']);
    $count = $woocommerce->cart->cart_contents_count + 1;
    $categories = array();
    $category_terms =  get_the_terms($product->get_id(), 'product_cat');
    foreach ($category_terms as $category_term) {
        $categories[] = $category_term->name;
    }

    $item_data = array(
        'item_name' => $product->get_name(),
        'item_id' => $product->get_sku() ? $product->get_sku() : ('#' . $product->get_id()),
        'price' => $product->get_price(),
        'item_brand' => 'Green Doctor',
        'item_category' => implode(', ', array_filter($categories)),
        'quantity' =>  '1',
        'index' => $count
    );

    wp_send_json($item_data);
}

// //Quitar del carrito
add_action( 'woocommerce_after_cart', 'gdh_ga_remove_from_cart' );
add_action( 'woocommerce_after_mini_cart', 'gdh_ga_remove_from_cart' );
function gdh_ga_remove_from_cart()
{
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    $cart = array();
    $item_count = 0;
    foreach ($items as $item) {
        $product = $item['data'];
        $item_count++;
        $categories = array();
        $category_terms = get_the_terms($product->get_id(), 'product_cat');
        foreach ($category_terms as $category_term) {
            $categories[] = $category_term->name;
        }
        $_item = array(
            'item_name' => $product->get_name(),
            'item_id' => $product->get_sku() ? $product->get_sku() : ('#' . $product->get_id()),
            'price' => $product->get_price(),
            'item_brand' => 'Green Doctor',
            'item_category' => implode(', ', array_filter($categories)),
            'quantity' =>  $item['quantity'],
            'index' => $item_count
        );
        $cart[$product->get_id()] = $_item;
    }
?>
    <script>
        (function($) {
            cart_items = JSON.parse('<?php echo json_encode($cart); ?>');
            removed_items = [];
            $(document.body).on('click', '.remove', function() {
                id = $(this).data('product_id');
                if (removed_items.includes(id)) return;
                dataLayer.push({
                    'event': 'remove_from_cart',
                    'ecommerce': {
                        'items': [{
                            'item_name': cart_items[id]['item_name'],
                            'item_id': cart_items[id]['item_id'],
                            'price': cart_items[id]['price'],
                            'item_brand': cart_items[id]['item_brand'],
                            'item_category': cart_items[id]['item_category'],
                            'index': cart_items[id]['index'],
                            'quantity': cart_items[id]['quantity']
                        }]
                    }
                });
                removed_items.push(id);
            });
        })(jQuery);
    </script>
<?php
}

/**
 * begin_checkout
 */

add_action('woocommerce_after_checkout_form', 'gdh_set_checkout_ga_code');
function gdh_set_checkout_ga_code()
{
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    $cart = array();
    $item_count = 0;
    foreach ($items as $item) {
        $product = $item['data'];
        $item_count++;
        $category_terms = get_the_terms($product->get_id(), 'product_cat');
        $categories = array();
        foreach ($category_terms as $category_term) {
            $categories[] = $category_term->name;
        }
        $_item = array(
            'item_name' => $product->get_name(),
            'item_id' => $product->get_sku() ? $product->get_sku() : ('#' . $product->get_id()),
            'price' => $product->get_price(),
            'item_brand' => 'Green Doctor',
            'item_category' => implode(', ', array_filter($categories)),
            'quantity' =>  $item['quantity'],
            'index' => $item_count
        );
        $cart[] = $_item;
    }


    $ga_code = "dataLayer.push({
        'event': 'begin_checkout',
        'ecommerce': {
            'items':" . json_encode($cart) . "
        }
    });";
    echo '<script>' . $ga_code . '</script>';
}

//Vista de detalle de producto
add_action('woocommerce_after_single_product', 'gdh_ga_product_detail');
function gdh_ga_product_detail()
{
    global $product;
    if (empty($product)) {
        return;
    }
    $categories = array();
    $category_terms = get_the_terms($product->get_id(), 'product_cat');
    foreach ($category_terms as $category_term) {
        $categories[] = $category_term->name;
    }

    wc_enqueue_js("
        dataLayer.push({
            'event': 'view_item',
            'ecommerce': {
            'items': [{
                'item_name': '" . esc_js($product->get_title()) . "',
                'item_id': '" . esc_js($product->get_sku() ? $product->get_sku() : ('#' . $product->get_id())) . "',
                'price': '" . esc_js($product->get_price()) . "',
                'item_brand': 'Green Doctor',
                'item_category': '" . esc_js(implode(', ', array_filter($categories))) . "',
                'quantity': '1',
                'index': '1',
                }]
            }
        });"
    );
}

add_action( 'woocommerce_order_status_changed', 'ga_measurement_protocol_transaction', 10, 3 );
function ga_measurement_protocol_transaction($order_id, $old_status, $new_status){
    $status = array('processing', 'cancelled');
    if(!in_array($new_status, $status))
        return;

    $order = wc_get_order($order_id);
    $payment_methods = array('payu_latam_api_spei', 'payu_latam_sdk_stores');
    
    if (!$order || !in_array($order->get_payment_method(), $payment_methods))
        return;
    
    $items = array();
    if ($order->get_items()) {
        foreach ($order->get_items() as $item) {
            $items[] = gdh_ga_add_item($item);
        }
    }

    $hit_body = array(
        'client_id' => bin2hex(openssl_random_pseudo_bytes(32)),
        'events' => array(
            'params' => array(
                'affiliation'     => 'Woocommerce',
                'coupon'   => implode(', ', $order->get_coupon_codes()),
                'currency'   => $order->get_currency(),
                'shipping'    => number_format( (float) $order->get_shipping_total(), wc_get_price_decimals(), '.', '' ),
                'transaction_id'    => $order_id,
                'value'    => number_format( (float) $order->get_total() - (float) $order->get_shipping_total(), wc_get_price_decimals(), '.', '' ),
                'items' => $items
            )
        )
    );

    if($new_status == 'processing'){
        $hit_body['events']['name'] = 'SOLD';
    }elseif($new_status == 'cancelled'){
        $hit_body['events']['name'] = 'CANCELADO';
    }
        
    // debug
    // $logger = wc_get_logger();
    // $logger->info( 'Change status data', array( 'source' => 'g-analytics' ) );
    // $logger->info( wc_print_r( $hit_body , true), array( 'source' => 'g-analytics' ) );
    
    ga_measurement_protocol_do_request($hit_body);

}

add_action( 'woocommerce_order_fully_refunded', 'ga_measurement_protocol_refund', 10, 2 ); 
function ga_measurement_protocol_refund( $order_id, $refund_id ) { 
    $payment_methods = array('payu_latam_api_spei', 'payu_latam_sdk_stores');
    $order = wc_get_order($order_id);

    if (!$order || !in_array($order->get_payment_method(), $payment_methods))
        return;

    $items = array();
    if ($order->get_items()) {
        foreach ($order->get_items() as $item) {
            $items[] = gdh_ga_add_item($item);
        }
    }

    $hit_body = array(
        'client_id' => bin2hex(openssl_random_pseudo_bytes(32)),
        'events' => array(
            'name' => 'REFUND',
            'params' => array(
                'affiliation'     => 'Woocommerce',
                'coupon'   => implode(', ', $order->get_coupon_codes()),
                'currency'   => $order->get_currency(),
                'shipping'    => number_format( (float) $order->get_shipping_total(), wc_get_price_decimals(), '.', '' ),
                'transaction_id'    => $order_id,
                'value'    => number_format( (float) $order->get_total() - (float) $order->get_shipping_total(), wc_get_price_decimals(), '.', '' ),
                'items' => $items
            )
        )
    );
    // debug
    // $logger = wc_get_logger();
    // $logger->info( 'Full refund data', array( 'source' => 'g-analytics' ) );
    // $logger->info( wc_print_r( $hit_body, true ), array( 'source' => 'g-analytics' ) );
    
    ga_measurement_protocol_do_request($hit_body);
}

add_action( 'woocommerce_order_partially_refunded', 'ga_measurement_protocol_partial_refund', 10, 2 ); 
function ga_measurement_protocol_partial_refund( $order_id, $refund_id ) { 
    $order = wc_get_order($order_id);
    $payment_methods = array('payu_latam_api_spei', 'payu_latam_sdk_stores');
    
    if (!$order || !in_array($order->get_payment_method(), $payment_methods)) {
        return;
    }
    $refund = new WC_Order_Refund( $refund_id );

    $items = array();
    if ($refund->get_items()) {
        foreach ($refund->get_items() as $item) {
            $items[] = gdh_ga_add_item($item);
        }
    }

    $hit_body = array(
        'client_id' => bin2hex(openssl_random_pseudo_bytes(32)),
        'events' => array(
            'name' => 'REFUND',
            'params' => array(
                'affiliation'     => 'Woocommerce',
                'coupon'   => implode(', ', $order->get_coupon_codes()),
                'currency'   => $order->get_currency(),
                'shipping'    => number_format( (float) $order->get_shipping_total(), wc_get_price_decimals(), '.', '' ),
                'transaction_id'    => $order_id,
                'value'    => number_format( (float) $order->get_total() - (float) $order->get_shipping_total(), wc_get_price_decimals(), '.', '' ),
                'items' => $items
            )
        )
    );

    // debug
    // $logger = wc_get_logger();
    // $logger->info( 'Partial refund data', array( 'source' => 'g-analytics' ) );
    // $logger->info( wc_print_r( $hit_body, true ), array( 'source' => 'g-analytics' ) );
    
    ga_measurement_protocol_do_request($hit_body);
}

function ga_measurement_protocol_do_request($hit_body){
    $measurement_id = 'G-8KRZQSYWTY';
    $api_secret = 'rpobQJG4St-8UGT96WE2sQ';
    $url = 'https://www.google-analytics.com/mp/collect?measurement_id='.$measurement_id.'&api_secret='.$api_secret;
    
    $response = wp_remote_post( $url, array(
        'method' => 'POST',
        'data_format' => 'body',
        'headers' => array('Content-Type' => 'application/json; charset=utf-8'),
        'body' => json_encode($hit_body)
        )
    );

    
    if ( is_wp_error( $response ) ) {
        $logger = wc_get_logger();
        $logger->info( 'Error response', array( 'source' => 'g-analytics' ) );
        $logger->info( wc_print_r( $response, true ), array( 'source' => 'g-analytics' ) );
        return false;
    }else {
        // $logger = wc_get_logger();
        // $logger->info( 'Success response', array( 'source' => 'g-analytics' ) );
        // $logger->info( wc_print_r( $response, true ), array( 'source' => 'g-analytics' ) );
        return true;
    }
}