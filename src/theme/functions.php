<?php
/**
 * Green Doctor Home Theme functions and definitions
 * 
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Green Doctor Home
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_GREEN_DOCTOR_HOME_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

    wp_enqueue_style( 'green-doctor-home-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_GREEN_DOCTOR_HOME_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

/**
 * Subir imágenes WebP 
 */

function mi_nuevo_mime_type( $existing_mimes ) {
    // añade webp a la lista de mime types
    $existing_mimes['webp'] = 'image/webp';
    // devuelve el array a la funcion con el nuevo mime type
    return $existing_mimes;
}
add_filter( 'mime_types', 'mi_nuevo_mime_type' );

/**
 * Subir imágenes SVG 
 */

function dmc_add_svg_mime_types($mimes) {
    if ( current_user_can('administrator') ){
        $mimes['svg'] = 'image/svg+xml';    
    }
    return $mimes;
}
add_filter('upload_mimes', 'dmc_add_svg_mime_types');

/**
  * Modificaciones a menu de mi cuenta
  * @link https://ayudawp.com/personalizar-mi-cuenta-woocommerce/
  */

 function my_account_menu_order() {
    $menuOrder = array(
        'dashboard'          => __( 'Tu Cuenta', 'woocommerce' ),
        'orders'             => __( 'Mis Compras', 'woocommerce' ),
        'edit-account'      => __( 'Mis Datos', 'woocommerce' ),                
        'customer-logout'    => __( 'Salir', 'woocommerce' ),
    );
    return $menuOrder;
 }
 add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

/* Unificando contenido de pestañas Editar Cuenta y Direcciones */
 
add_filter( 'woocommerce_account_menu_items', 'ayudawp_mover_direcciones', 999 );
  
function ayudawp_mover_direcciones( $items ) {
unset($items['edit-address']);
return $items;
}
 
// Luego mostramos el contenido de las direcciones en otra pestaña (edit-account en este ejemplo)
 
add_action( 'woocommerce_account_edit-account_endpoint', 'woocommerce_account_edit_address' );

/**
 * Mostrar Imagen en Detalles Del Pedido  
 * @link https://therichpost.com/add-product-image-woocommerce-account-order-view/
 */

// Display the product thumbnail in order view pages
add_filter( 'woocommerce_order_item_name', 'display_product_image_in_order_item', 20, 3 );
function display_product_image_in_order_item( $item_name, $item, $is_visible ) {
    // Targeting view order pages only
    if( is_wc_endpoint_url( 'view-order' ) ) {
        $product       = $item->get_product(); // Get the WC_Product object (from order item)
        $product_image = $product->get_image(array( 120, 120)); // Get the product thumbnail (from product object)
        $item_name     = '<div class="item-thumbnail">' . $product_image . '</div>' . $item_name;
    }
    return $item_name;
}

/*
 * Cambiar título de busqueda
 * */

add_filter( 'astra_the_search_page_title', 'my_function', 10 ); 

function my_function() {
    return sprintf( __( 'Resultados para: %s', 'astra' ), '<span>' . get_search_query() . '</span>' );
}


// Analytics by dgk
require(get_stylesheet_directory().'/g-analytics.php');